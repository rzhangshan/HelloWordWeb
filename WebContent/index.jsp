<%@page import="java.util.Date"%>
<%@page import="java.sql.*"%>
<%@page import="com.hellowordweb.scmysql.SCMysqlSchool"%>
<%@page import="com.hellowordweb.test.TestWeb"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HelloWordWeb</title>
</head>

<body style="margin:0px;" >
	<div align="center">
		<%@ include file="top.jsp" %><br>
	</div>
	
	<div align="center">
	<form action="http://www.baidu.com/baidu" target="_blank">
		<input name="wd" size="30">
		<input type="submit" value="百度搜索" >
	</form>
	</div>
	
	<div align="center">
		<a href="http://www.baidu.com" target="_blank">百度</a>
		<br>
		<a href="test.jsp?id=1" >删除</a>
		<h3> 数据信息表</h3>
		<table border="1">
			<caption>数据信息表</caption>
			<% 
			try {
				TestWeb testWeb = new TestWeb();
				testWeb.printHelloWord();
				testWeb.scMysqlOpertion();
				
				Connection connection = SCMysqlSchool.connect();
				ResultSet rs = SCMysqlSchool.getSelectStudentResultSet(connection);
				int id,age;
				String username,pwd,sex,tel;
				%>
				<tr>
					<th>ID</th><th>用户名</th><th>密码</th><th>性别</th><th>年龄</th><th>电话</th>
				</tr>
				<%
				while (rs.next())
				{
					id = rs.getInt(1);
					username = rs.getString("username");
					pwd = rs.getString("password");
					sex = rs.getString("sex");
					age = rs.getInt("age");
					tel = rs.getString("tel");
				%>
				<tr>
					<td align="center"><%=id %></td>
		    		<td align="center"><%=username %></td>
		    		<td align="center"><%=pwd %></td>
		    		<td align="center"><%=sex %></td>
		    		<td align="center"><%=age %></td>
		    		<td align="center"><%=tel %></td>
		    	</tr>
				<%
				}
			} catch (ClassNotFoundException e) {
				%>
		    	<tr>
					<th>Mysql驱动加载失败！</th>
				</tr>
		    	<%
			} catch (SQLException e) {
				%>
				<tr>
					<th>Mysql连接失败！</th>
				</tr>
		    	<%
			}
				%>
		</table>
	</div>
	
	<script type="text/javascript"> 
		function realSysTime(){
			var dnow = new Date();
			dhours = dnow.getHours();
			dminutes = dnow.getMinutes();
			dseconds = dnow.getSeconds();
		//	document.write("Browser Time:" + dhours + ":" + dminutes + ":" + dseconds);
		} 
		window.onload=function(){ 
			window.setInterval("realSysTime()",1000); //实时获取并显示系统时间 
		} 
	</script>
	
	<%!
		// 只初始化，调用时才执行。
		int counter = 0;
		public int getCounter()
		{
			counter ++;
			return counter;
		}
	%>
	<div align="center">
		<br>欢迎您第<%=getCounter() %>位用户
		<%
			SimpleDateFormat foo = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			out.write("   服务器时间：" + foo.format(new Date()) + "\n");
			//HelloWord helloWord = new HelloWord();
			//helloWord.PrintHelloword();
		%>
	</div>
	<!-- audio src="alarm.mp3" >"您的浏览器不支持&lt;audio&gt;标记！"</audio> -->
	<div align="center">
		<%@ include file="bottom.jsp" %><br>
	</div>
</body>
</html>