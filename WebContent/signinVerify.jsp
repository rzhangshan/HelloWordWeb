<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="com.hellowordweb.scmysql.SCMysqlSchool"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:useBean id="us" class="com.hellowordweb.usermange.SigninVerify" scope="request"/>
	<jsp:setProperty property="*" name="us"/>
	<%
		String name=us.getUsername();
		String password=us.getPassword();
		String cpassword=us.getCpassword();
		String sex=us.getSex();
		int age=us.getAge();
		String tel=us.getTel();
		
		boolean frepeat = false;
		try {
			Connection connection = SCMysqlSchool.connect();
			ResultSet rs = SCMysqlSchool.getSelectStudentResultSet(connection);
			while (rs.next())
			{
				if (name.equals(rs.getString("username")))
				{
					frepeat = true;
					break;
				}
			}
			if (frepeat)
			{
				%>
					用户名已注册！
				<%
			}
			else if (!cpassword.equals(password))
			{
				%>
					两次密码输入不相同；
				<%
			}	
			else if (name.equals("") || password.equals("") || age == 0 || age > 200 || 
					!sex.equals("男") && !sex.equals("女"))
			{
				//<jsp:forward page="/false.jsp"/>
				//<jsp:forward page="/success.jsp"/>
				%>
					用户名注册失败！
				<%
			}
			else
			{
				Date date = new Date();
			    long times = date.getTime();
				SCMysqlSchool.insertStudentsValues(connection, name, password, sex, age, tel, times);
				%>
					用户名注册成功！
				<%
			}
		} catch (ClassNotFoundException e) {
			%>
	    	数据库错误，用户名注册失败！
	    	<%
		} 
		catch (SQLException e) {
			%>
			数据库错误，用户名注册失败！
	    	<%
		}
	%>
	<ul><li><a href="index.jsp">[ 返回 ]</a></li></ul>
</body>
</html>