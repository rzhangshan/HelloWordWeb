package com.hellowordweb.scmysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SCMysqlSchool {
	
	/**
	 * @brief 连接默认MySQL数据库
	 * @return 连接接口 null-连接无效
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static Connection connect() throws ClassNotFoundException, SQLException {
//		String url = "jdbc:mysql://192.168.1.110:3306/school?characterEncoding=utf-8";
		String url = "jdbc:mysql://localhost:3306/school?characterEncoding=utf-8";
		String user = "root";
		String password = "zhangshan";
		Connection connection = null;
		
		Class.forName("com.mysql.jdbc.Driver");
		
		connection = DriverManager.getConnection(url, user, password);
		
		return connection;
	}

	/**
	 * @brief 连接指定数据库
	 * @param ipAddr 数据库地址
	 * @param database 数据库名称
	 * @param username 用户名
	 * @param password 密码
	 * @return 连接信息 null-连接无效
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static Connection connect(String ipAddr, String database, String username, String password) throws ClassNotFoundException, SQLException {
		String url = "jdbc:mysql://" + ipAddr + ":3306/" + database;
		Connection connection = null;
		
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection(url, username, password);
		
		return connection;
	}
	
	/**
	 * @brief 删除数据表
	 * @param connection
	 * @param tablename
	 * @return
	 * @throws SQLException 
	 */
	public static boolean delTable(Connection connection, String tablename) throws SQLException {
		
		String sql = "drop table " + tablename;
		Statement statement = null;
		
		statement = connection.createStatement();
		statement.executeUpdate(sql);
		return true;
	}
	
	/**
	 * @brief 新建数据表
	 * @param connection
	 * @param tablename
	 * @return
	 * @throws SQLException 
	 */
	public static boolean addTable(Connection connection, String tablename) throws SQLException {
		
		String sql = "create table " + tablename 
				+ "("
				+ "id int unsigned not null auto_increment primary key,"
				+ "username char(8) not null,"
				+ "password char(10) not null,"
				+ "sex char(4) not null,"
				+ "age tinyint unsigned not null,"
				+ "tel char(13) null default '-'"
				+ ")";

		Statement statement = null;
		
		statement = connection.createStatement();
		statement.executeUpdate(sql);
		return true;
	}
	
	/**
	 * @brief 插入学生信息
	 * @param connection
	 * @param username
	 * @param password
	 * @param sex
	 * @param age
	 * @param tel
	 * @return
	 * @throws SQLException 
	 */
	public static boolean insertStudentsValues(Connection connection, String username, String password, String sex, int age, String tel,long times) throws SQLException {
		
		String sql = "insert into students(username,password,sex,age,tel,date)values(?,?,?,?,?,?)";
		
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, username);
		ps.setString(2, password);
		ps.setString(3, sex);
		ps.setInt(4, age);
		ps.setString(5, tel);
		ps.setDate(6, new Date(times));
		ps.executeUpdate();
		return true;
	}
	
	/**
	 * @brief 查询所有学生信息
	 * @param connection
	 * @return
	 * @throws SQLException 
	 */
	public static boolean selectStudentValues(Connection connection) throws SQLException {
		
		String selectsql = "select * from students";
		Statement statement = null;
		
		statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(selectsql);
		int id,age;
		String username,pwd,sex,tel;
		System.out.println("id\t用户名\t密码\t性别\t年龄\t电话");
		while (rs.next())
		{
			id = rs.getInt(1);
			username = rs.getString("username");
			pwd = rs.getString("password");
			sex = rs.getString("sex");
			age = rs.getInt("age");
			tel = rs.getString("tel");
			System.out.println(id + "\t" + username + "\t" + pwd + "\t" + sex + "\t" + age + "\t" + tel);
		}
		return true;
	}
	
	/**
	 * @brief 查询所有学生信息
	 * @param connection
	 * @return
	 * @throws SQLException 
	 */
	public static ResultSet getSelectStudentResultSet(Connection connection) throws SQLException {
		
		String selectsql = "select * from students";
		Statement statement = null;
		
		statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(selectsql);
//		int id,age;
//		String username,pwd,sex,tel;
//		System.out.println("id\t用户名\t密码\t性别\t年龄\t电话");
//		while (rs.next())
//		{
//			id = rs.getInt(1);
//			username = rs.getString("username");
//			pwd = rs.getString("password");
//			sex = rs.getString("sex");
//			age = rs.getInt("age");
//			tel = rs.getString("tel");
//			System.out.println(id + "\t" + username + "\t" + pwd + "\t" + sex + "\t" + age + "\t" + tel);
//		}
		return rs;
	}
}
