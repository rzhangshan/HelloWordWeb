package com.hellowordweb.usermange;

import java.io.UnsupportedEncodingException;

public class LoginVerify {
	private String username = "";
	private String password = "";
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) throws UnsupportedEncodingException {
		this.username = new String(username.getBytes("ISO-8859-1"), "UTF-8");
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
