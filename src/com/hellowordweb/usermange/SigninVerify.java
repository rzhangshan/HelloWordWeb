package com.hellowordweb.usermange;

import java.io.UnsupportedEncodingException;

public class SigninVerify {

	private String username = "";
	private String password = "";
	private String cpassword = "";
	private String sex = "";
	private int age = 0;
	private String tel = "";
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) throws UnsupportedEncodingException {
		this.username = new String(username.getBytes("ISO-8859-1"), "UTF-8");
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCpassword() {
		return cpassword;
	}
	public void setCpassword(String cpassword) {
		this.cpassword = cpassword;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) throws UnsupportedEncodingException {
		this.sex = new String(sex.getBytes("ISO-8859-1"), "UTF-8");
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
}
