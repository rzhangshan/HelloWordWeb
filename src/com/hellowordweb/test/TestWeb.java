package com.hellowordweb.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import com.hellowordweb.scmysql.SCMysqlSchool;

public class TestWeb {
	public void TestWebConnect() throws ClassNotFoundException, SQLException {
		Connection connection = SCMysqlSchool.connect();
		SCMysqlSchool.getSelectStudentResultSet(connection);
		SCMysqlSchool.delTable(connection, "students");
		SCMysqlSchool.addTable(connection, "students");
		SCMysqlSchool.insertStudentsValues(connection, "����", "123456789", "��", 12, "13887888888",new Date().getTime());
		SCMysqlSchool.selectStudentValues(connection);
		System.out.println("The program run finish!");
	}
	
	public void scMysqlOpertion() 
	{
		System.out.println("The program run finish!");
	}
	
	public void printHelloWord()
	{
		System.out.println("Hello Word!");
	}
	
}
